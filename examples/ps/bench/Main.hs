{-# LANGUAGE NumericUnderscores #-}

module Main (main) where

import Control.Monad (replicateM)
import Control.Monad.IO.Class (MonadIO)
import Criterion.Main (Benchmark, bench, bgroup, defaultMain, env, nfIO)
import System.Random (mkStdGen)
import System.Random.Stateful (newIOGenM, uniformRM)
import Text.Printf (printf)

import PS (PS)
import PS.IO (delayedExec)
import PS.Matrix (Elem, inverse, newSquareMatrixFromLists)
import PS.Runners (Runner(..), allRunners, withRunInIO)

main :: IO ()
main = defaultMain
  [ benchMatrixOfSide 5
  , benchMatrixOfSide 10
  ]

seed :: Int
seed = -37

genMatrix :: MonadIO m => Int -> Int -> m [[Elem]]
genMatrix height width = do
  genM <- newIOGenM (mkStdGen seed)
  replicateM height (replicateM width (uniformRM (0.5, 100) genM))

benchInverseOnRunner :: [[Elem]] -> Runner -> Benchmark
benchInverseOnRunner l runner =
  (bench (runName runner)
    (nfIO (withRunInIO runner
            (\runInIO -> runInIO (matrixInverse l)))))

-- | The delay is so big because the OS timer is not precise enough to
-- deal with, for example, 15 microseconds.
execDelayInMicroSecs :: Int
execDelayInMicroSecs = 1_500

benchAll :: (Runner -> Benchmark) -> [Benchmark]
benchAll bencher = map bencher (allRunners (delayedExec execDelayInMicroSecs))

matrixInverse :: [[Elem]] -> PS ref arr ()
matrixInverse l = do
  m <- newSquareMatrixFromLists l
  _ <- inverse m
  pure ()

benchMatrixOfSide :: Int -> Benchmark
benchMatrixOfSide n =
  (env (genMatrix n n) $ \lMatrix ->
      (bgroup (printf "inverse of %dx%d matrix" n n)
        (benchAll (benchInverseOnRunner lMatrix))))
