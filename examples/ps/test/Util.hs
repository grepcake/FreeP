{-# LANGUAGE RankNTypes #-}

module Util
  ( checkOnRunners
  , checkOnAllRunners
  ) where

import Control.Monad.IO.Class (MonadIO)
import Data.Foldable (for_)
import qualified Test.Hspec as H

import PS.IO (exec)
import PS.Runners (RunInIO, Runner(..), allRunners, withRunInIO)

checkOnRunners
  :: (MonadIO m, H.Example a)
  => [Runner] -> (RunInIO m -> a) -> H.SpecWith (H.Arg a)
checkOnRunners runners expectation = do
  for_ runners $ \runner -> do
    H.it ("works with the '" <> runName runner <> "' runner") $
      withRunInIO runner expectation

checkOnAllRunners
  :: (MonadIO m, H.Example a) => (RunInIO m -> a) -> H.SpecWith (H.Arg a)
checkOnAllRunners = checkOnRunners (allRunners exec)
