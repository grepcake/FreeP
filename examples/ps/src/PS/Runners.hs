{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}

module PS.Runners
  ( RunInPSIO
  , RunInIO
  , Runner(..)
  , withRunInIO
  , allRunners
  , parSeqRunner
  , parConcRunner
  , parTgRunner
  , graphSeqRunner
  , graphConcRunner
  , graphTgRunner
  ) where

import Control.Concurrent.Async.Pool (TaskGroup, withTaskGroup)
import Control.Monad.IO.Class (MonadIO(..))
import FreeP (FreeP)
import MultiExec
  (allConcurrent, allConcurrentEff, inTaskGroup, inTaskGroupEff, sequential, sequentialEff)
import Runners (runParallelizable, runWithDependenceGraph)

import PS.IO (Exec, PSIO, PSIOEff, buildGraph, runPSIO)

type RunInPSIO = forall a. FreeP PSIOEff a -> PSIO a
type RunInIO m = forall a. FreeP PSIOEff a -> m a

data Runner = Runner
  { runName :: String
  , runAction :: TaskGroup -> RunInPSIO
  }

nThreads :: Int
nThreads = 2

withRunInIO :: forall m r. MonadIO m => Runner -> (RunInIO m -> r) -> r
withRunInIO runner inner = inner (mkRunInIO runner)
  where
    mkRunInIO :: Runner -> RunInIO m
    mkRunInIO r freep =
      liftIO (withTaskGroup nThreads (\tg -> runPSIO (runAction r tg freep)))

parSeqRunner :: Exec -> Runner
parSeqRunner exec =
  Runner "par|seq"
    (\_tg -> runParallelizable (sequential exec))

parConcRunner :: Exec -> Runner
parConcRunner exec =
  Runner "par|allConc"
    (\_tg -> runParallelizable (allConcurrent exec))

parTgRunner :: Exec -> Runner
parTgRunner exec =
  Runner "par|tg"
    (\tg -> runParallelizable (inTaskGroup tg exec))

graphSeqRunner :: Exec -> Runner
graphSeqRunner exec =
  Runner "graph|seq"
    (\_tg -> runWithDependenceGraph buildGraph (sequentialEff exec))

graphConcRunner :: Exec -> Runner
graphConcRunner exec =
  Runner "graph|allConc"
    (\_tg -> runWithDependenceGraph buildGraph (allConcurrentEff exec))

graphTgRunner :: Exec -> Runner
graphTgRunner exec =
  Runner "graph|tg"
    (\tg -> runWithDependenceGraph buildGraph (inTaskGroupEff tg exec))

allRunners :: Exec -> [Runner]
allRunners exec =
  [ parSeqRunner exec
  , parConcRunner exec
  , parTgRunner exec
  , graphSeqRunner exec
  , graphConcRunner exec
  , graphTgRunner exec
  ]
