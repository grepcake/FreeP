{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module PS.Matrix
  ( Matrix
  , Elem
  , SquareMatrix
  , IsMatrix (..)
  , matrixSide
  , newZeroMatrix
  , newZeroSquareMatrix
  , newMatrixFromLists
  , safeNewMatrixFromLists
  , newSquareMatrixFromLists
  , safeNewSquareMatrixFromLists
  , hstack
  , inverse
  , to2dList
  ) where

import Control.Monad (foldM, unless, when)
import Control.Monad.Except (ExceptT, lift, runExceptT, throwError)
import Data.Foldable (for_)

import PS (PS, newArray, newArrayFromList, newRef, readArray, readRef, writeArray, writeRef)

type Elem = Double

data Matrix arr = Matrix
  { matArray :: arr Elem
  , matHeight :: Int
  , matWidth :: Int
  }

data SquareMatrix arr = SquareMatrix
  { sqmatArray :: arr Elem
  , sqmatSide :: Int
  }

class IsMatrix mat where
  readMatrix :: mat arr -> Int -> Int -> PS ref arr Elem
  writeMatrix :: mat arr -> Int -> Int -> Elem -> PS ref arr ()
  matrixHeight :: mat arr -> Int
  matrixWidth :: mat arr -> Int
  to1dArray :: mat arr -> PS ref arr (arr Elem)
  toMatrix :: mat arr -> PS ref arr (Matrix arr)
  toMatrix mat = do
    arr <- to1dArray mat
    pure Matrix
      { matArray = arr
      , matHeight = matrixHeight mat
      , matWidth = matrixWidth mat
      }

instance IsMatrix Matrix where
  readMatrix Matrix{..} row col = readArray matArray (row * matWidth + col)
  writeMatrix Matrix{..} row col = writeArray matArray (row * matWidth + col)
  matrixHeight = matHeight
  matrixWidth = matWidth
  to1dArray = pure . matArray
  toMatrix = pure

instance IsMatrix SquareMatrix where
  readMatrix SquareMatrix{..} row col = readArray sqmatArray (row * sqmatSide + col)
  writeMatrix SquareMatrix{..} row col = writeArray sqmatArray (row * sqmatSide + col)
  matrixHeight = sqmatSide
  matrixWidth = sqmatSide
  to1dArray = pure . sqmatArray

modifyMatrix
  :: IsMatrix mat => mat arr -> Int -> Int -> (Elem -> Elem) -> PS ref arr ()
modifyMatrix mat i j f = do
  v <- readMatrix mat i j
  writeMatrix mat i j (f $! v)

matrixSide :: SquareMatrix arr -> Int
matrixSide = sqmatSide

newZeroMatrix :: Int -> Int -> PS ref arr (Matrix arr)
newZeroMatrix height width = do
  arr <- newArray (height * width) 0
  pure Matrix{ matArray = arr, matHeight = height, matWidth = width }

newZeroSquareMatrix :: Int -> PS ref arr (SquareMatrix arr)
newZeroSquareMatrix side = do
  arr <- newArray (side * side) 0
  pure SquareMatrix{ sqmatArray = arr, sqmatSide = side }

newIdentity :: Int -> PS ref arr (SquareMatrix arr)
newIdentity side = do
  mat <- newZeroSquareMatrix side
  for_ [0..side - 1] $ \i -> writeMatrix mat i i 1
  pure mat

newMatrixFromLists :: [[Elem]] -> PS ref arr (Matrix arr)
newMatrixFromLists list = do
  eitherRes <- runExceptT (safeNewMatrixFromLists list)
  case eitherRes of
    Left err -> error ("newMatrixFromLists: " <> err)
    Right res -> pure res

safeNewMatrixFromLists :: [[Elem]] -> ExceptT String (PS ref arr) (Matrix arr)
safeNewMatrixFromLists list = do
  let height = length list
  width <- extractWidth
  arr <- lift (newArrayFromList (concat list))
  pure Matrix{ matArray = arr, matHeight = height, matWidth = width }
  where
    extractWidth = case list of
      [] -> throwError "safeNewMatrixFromLists: the list is empty"
      (row : rows) -> case length row of
        l | l <= 0
            -> throwError "safeNewMatrixFromLists: there is an empty row"
          | any (\r -> length r /= l) rows
            -> throwError "safeNewMatrixFromLists: some rows have different length"
          | otherwise
            -> pure l

toSquareMatrix :: IsMatrix mat => mat arr -> Maybe (PS ref arr (SquareMatrix arr))
toSquareMatrix mat | matrixHeight mat /= matrixWidth mat = Nothing
toSquareMatrix mat = Just $ do
  let side = matrixHeight mat
  arr1d <- to1dArray mat
  pure SquareMatrix{ sqmatSide = side, sqmatArray = arr1d }

newSquareMatrixFromLists :: [[Elem]] -> PS ref arr (SquareMatrix arr)
newSquareMatrixFromLists list = do
  eitherRes <- runExceptT (safeNewSquareMatrixFromLists list)
  case eitherRes of
    Left err -> error ("newSquareMatrixFromLists: " <> err)
    Right res -> pure res

safeNewSquareMatrixFromLists
  :: forall ref arr. [[Elem]] -> ExceptT String (PS ref arr) (SquareMatrix arr)
safeNewSquareMatrixFromLists list = do
  matrix <- safeNewMatrixFromLists @ref @arr list
  case toSquareMatrix matrix of
    Nothing -> throwError nonSquareListMsg
    Just sqMatrix -> lift sqMatrix
  where
    nonSquareListMsg = "safeNewSquareMatrixFromLists: can't make a square matrix out of a non-square list"

hstack :: (IsMatrix mat1, IsMatrix mat2) => mat1 arr -> mat2 arr -> PS ref arr (Matrix arr)
hstack mat1 mat2 = do
  let height = matrixHeight mat1
  let width1 = matrixWidth mat1
  let width = width1 + matrixWidth mat2
  mat <- newZeroMatrix height width

  for_ [0..height - 1] $ \i -> do
    for_ [0..width1 - 1] $ \j ->
      readMatrix mat1 i j >>= writeMatrix mat i j
    for_ [width1..width - 1] $ \j ->
      readMatrix mat2 i (j - width1) >>= writeMatrix mat i j

  pure mat

inverse :: SquareMatrix arr -> PS ref arr (SquareMatrix arr)
inverse mat = do
  let side = sqmatSide mat
  idmat <- newIdentity side
  m <- hstack mat idmat
  makeRref m

  result <- newZeroSquareMatrix side
  for_ [0..side - 1] $ \row ->
    for_ [side..2 * side - 1] $ \col ->
      writeMatrix result row (col - side) =<< readMatrix m row col
  pure result

makeRref :: IsMatrix mat => mat arr -> PS ref arr ()
makeRref mat = do
  pivotRowRef <- newRef 0
  for_ [0..min height width - 1] $ \pivotCol -> do
    pivotRow <- readRef pivotRowRef
    maxRow <- findMaxAbsValueRow pivotRow pivotCol
    v <- readMatrix mat maxRow pivotCol
    when (v /= 0) $ do
      swapRows mat maxRow pivotRow
      divideRowBy mat pivotRow v
      reduceOtherRows pivotRow pivotCol
      writeRef pivotRowRef (pivotRow + 1)
  where
    height = matrixHeight mat
    width = matrixWidth mat
    findMaxAbsValueRow from col = foldM maxByAbsValue from [from..height - 1]
      where
        maxByAbsValue i j = do
          valI <- readMatrix mat i col
          valJ <- readMatrix mat j col
          pure (if abs valJ > abs valI then j else i)

    reduceOtherRows pivotRow pivotCol = do
      for_ [0..height - 1] $ \row -> unless (row == pivotRow) $ do
        coef <- readMatrix mat row pivotCol
        for_ [pivotCol..width - 1] $ \col -> do
          pivotValue <- readMatrix mat pivotRow col
          modifyMatrix mat row col (subtract (pivotValue * coef))


swapRows :: IsMatrix mat => mat arr -> Int -> Int -> PS ref arr ()
swapRows mat i j =
  for_ [0..matrixWidth mat - 1] (\k -> swapCells mat (i, k) (j, k))

swapCells :: IsMatrix mat => mat arr -> (Int, Int) -> (Int, Int) -> PS ref arr ()
swapCells mat (r1, c1) (r2, c2) = do
  val1 <- readMatrix mat r1 c1
  val2 <- readMatrix mat r2 c2
  writeMatrix mat r1 c1 val2
  writeMatrix mat r2 c2 val1

divideRowBy :: IsMatrix mat => mat arr -> Int -> Elem -> PS ref arr ()
divideRowBy mat row v =
  for_ [0..matrixWidth mat - 1] (\col -> modifyMatrix mat row col (/ v))

to2dList :: IsMatrix mat => mat arr -> PS ref arr [[Elem]]
to2dList mat = traverse rowToList [0..matrixHeight mat - 1]
  where
    rowToList i = traverse (\j -> readMatrix mat i j) [0..matrixWidth mat - 1]
