{-

By Simon Marlow, the code for the "There is no Fork: an Abstraction
for Efficient,Concurrent, and Concise Data Access" paper.

https://github.com/simonmar/haxl-icfp14-sample-code

-}

module MyFetch where

import FreeP (FreeP)

import Types

type Fetch = FreeP Request
