module Main where

import qualified MultiExec (sequential)
import Runners (runParallelizable)

import Blog (Html(..), blog)
import MockData (requestVal)

main :: IO ()
main = do
  let exec = pure . requestVal
  let multiExec = MultiExec.sequential exec
  Html <- runParallelizable multiExec blog
  putStrLn "ok"
