{-# LANGUAGE GADTs #-}
{-# LANGUAGE ApplicativeDo #-}

import Control.Applicative.Free (Ap(..), liftAp)
import Control.Monad (when)
import qualified Control.Monad.Free.Ap as GivenAp (Free(..), liftF)
import Data.Foldable (for_)
import Data.List (intercalate)

import Lib (Operation(..), checkPassword, printOp)

-- type FreeAp = Ap

-- type Free f = Free' (FreeAp f)

-- data Free' f a where
--   Pure :: a -> Free' f a
--   Step :: f (Free' f a) -> Free' f a

-- instance Applicative f =>
--          Applicative (Free' f) where
--   pure = Pure
--   Pure f <*> a = fmap f a
--   Step f <*> Pure a =
--     Step (fmap (fmap ($ a)) f)
--   Step f <*> Step a =
--     Step (fmap (<*>) f <*> a)

type Free f = GivenAp.Free (Ap f)

type Lang = Free Operation
say :: String -> Lang ()
input :: Lang String

say s = GivenAp.liftF (liftAp (Say s))
input = GivenAp.liftF (liftAp Input)

code1 :: Lang ()
code1 = do
  say "Enter username"
  user <- input
  say ("Enter password for " <> user)
  pwd <- input
  when (checkPassword user pwd) $ do
    say ("Welcome, " <> user)

printAp :: Ap Operation a -> ([String], a)
printAp (Pure x) = ([], x)
printAp (Ap fx next) =
  let (rest, res) = printAp next
      (pres, arg) = printOp fx
  in (pres : rest, res arg)

printFree :: Free Operation () -> [[String]]
printFree (GivenAp.Pure _) = []
printFree (GivenAp.Free fx) =
  let (pres, arg) = printAp fx
  in pres : printFree arg

putCode :: Lang () -> IO ()
putCode code = do
  let seqOrder = printFree code
  for_ seqOrder $ \parOrder ->
    putStrLn (intercalate ", " parOrder)
  putStrLn ""

main :: IO ()
main = do
  putStrLn "The fraxl free monad"
  putStrLn "-----"
  putCode code1
