{-# LANGUAGE GADTs #-}
{-# LANGUAGE ApplicativeDo #-}

import Control.Monad (when)

import Lib (Operation(..), checkPassword, printOp)

data Free f a where
  Pure :: a -> Free f a
  Impure :: f x -> (x -> Free f a) -> Free f a

instance Functor (Free f) where
  fmap f (Pure x) = Pure (f x)
  fmap f (Impure fx next) = Impure fx (\x -> fmap f (next x))

instance Applicative (Free f) where
  pure = Pure
  Pure f <*> mx = fmap f mx
  Impure fx next <*> my = Impure fx (\x -> (next x) <*> my)

instance Monad (Free f) where
  Pure a >>= f = f a
  Impure fx next >>= f = Impure fx (\x -> next x >>= f)

type Lang = Free Operation
say :: String -> Lang ()
input :: Lang String

code1 :: Lang ()
code1 = do
  say "Enter username"
  user <- input
  say ("Enter password for " <> user)
  pwd <- input
  when (checkPassword user pwd) $ do
    say ("Welcome, " <> user)

say s = Impure (Say s) pure
input = Impure Input pure

printFree :: Free Operation () -> [String]
printFree (Pure _) = []
printFree (Impure fx next) =
  let (pres, arg) = printOp fx
  in pres : printFree (next arg)

main :: IO ()
main = do
  putStrLn "Conventional free monads\n-----"
  putStrLn (unlines (printFree code1))
