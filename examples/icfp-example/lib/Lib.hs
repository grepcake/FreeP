{-# LANGUAGE GADTs #-}

module Lib (Operation(..), printOp, checkPassword) where

import Data.List (isPrefixOf)

data Operation a where
  Say :: String -> Operation ()
  Input :: Operation String
  ReadTimestamp
    :: String -> Operation Integer
  UpdateTimestamp
    :: String -> Operation ()

printOp :: Operation a -> (String, a)
printOp (Say s)
  | "Enter username" `isPrefixOf` s  = ("Say \"user\"", ())
  | "Enter password" `isPrefixOf` s = ("Say \"pwd\"", ())
  | "Welcome" `isPrefixOf` s = ("Say \"welcome\"", ())
  | "Last logged" `isPrefixOf` s = ("Say \"last\"", ())
  | otherwise = ("Say " <> show s, ())
printOp Input = ("Input", "something")
printOp (ReadTimestamp _filepath) = ("ReadT", 1629192589)
printOp (UpdateTimestamp _filepath) = ("UpdT", ())

checkPassword :: String -> String -> Bool
checkPassword "me" "strong-password" = True
checkPassword "noob" "weak-password" = True
checkPassword "something" "something" = True
checkPassword _ _ = False
