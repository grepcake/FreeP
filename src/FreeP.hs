{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ExistentialQuantification #-}

module FreeP
  ( FreeAp(..)
  , liftFreeAp
  , runFreeAp
  , Free(..)
  , liftFree
  , foldFree
  , FreeP
  , liftFreeP
  , foldFreeP
  ) where

data FreeAp f a where
  PureA :: a -> FreeAp f a
  ImpureA :: f x -> FreeAp f (x -> a) -> FreeAp f a

instance Functor (FreeAp f) where
  fmap f (PureA x) = PureA (f x)
  fmap f (ImpureA fx next) = ImpureA fx (fmap (f .) next)

instance Applicative (FreeAp f) where
  pure = PureA

  (<*>) :: FreeAp f (a -> b) -> FreeAp f a -> FreeAp f b
  PureA f <*> rhs = fmap f rhs
  ImpureA fx next <*> rhs = ImpureA fx (flip <$> next <*> rhs)

liftFreeAp :: f a -> FreeAp f a
liftFreeAp fx = ImpureA fx (PureA id)

runFreeAp :: Applicative g => (forall x. f x -> g x) -> FreeAp f a -> g a
runFreeAp _ (PureA a) = pure a
runFreeAp exec (ImpureA fx next) = (\x f -> f x) <$> exec fx <*> runFreeAp exec next

data Free f a where
  Last :: f a -> Free f a
  Step :: f x -> (x -> Free f a) -> Free f a

instance Functor f => Functor (Free f) where
  fmap f (Last fa) = Last (fmap f fa)
  fmap f (Step fx next) = Step fx (fmap f . next)

instance Applicative f => Applicative (Free f) where
  pure x = Last (pure x)

  Last ff <*> Last fa = Last (ff <*> fa)
  Last ff <*> Step fx next = Step ((,) <$> ff <*> fx) (\(f, x) -> fmap f (next x))
  -- important: merging of effects happens only at the top (Last) of
  -- lhs, since semantic ordering is left-to-right (as with >>=)
  Step fx next <*> rhs = Step fx (\x -> next x <*> rhs)

instance Applicative f => Monad (Free f) where
  Last fa >>= rhs = Step fa rhs
  Step fx next >>= rhs = Step fx (\x -> next x >>= rhs)

liftFree :: f a -> Free f a
liftFree = Last

foldFree :: (Monad m, Applicative f) => (forall x. f x -> m x) -> Free f a -> m a
foldFree exec (Last fa) = exec fa
foldFree exec (Step fx next) = exec fx >>= (\x -> foldFree exec (next x))

type FreeP f = Free (FreeAp f)

liftFreeP :: f a -> FreeP f a
liftFreeP fa = liftFree (liftFreeAp fa)

foldFreeP :: Monad m => (forall x. f x -> m x) -> FreeP f a -> m a
foldFreeP exec = foldFree (runFreeAp exec)
